(function(angular) {
    var app = angular.module("app", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        $stateProvider.state({
            name: "home",
            url: "/home",
            templateUrl: "app/components/komponente/komponente.tpl.html",
            controller: "komponenteCtrl",
            controllerAs: "kc"
        });
        $urlRouterProvider.otherwise("/");
    }]);
})(angular);
