(function(angular) {
    var app = angular.module("app");

    app.controller("komponenteCtrl", ["$http", function($http) {
        var that = this;

        this.komponente = [];
        
        this.dobaviKomponente = function() {
            $http.get("/komponente").then(function(response) {
                that.komponente = response.data;
            }), function(response) {
                console.log("Greska " + response.status);
            }
        };

        this.dobaviKomponente();
    }]);
})