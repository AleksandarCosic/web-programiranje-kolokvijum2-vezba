import flask
import datetime
from flask import Flask
from flask import request


from flaskext.mysql import MySQL

from pymysql.cursors import DictCursor

from functools import wraps

mysql_db = MySQL(cursorclass=DictCursor)

app = Flask(__name__, static_url_path="")
app.secret_key = "sta god"

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '' 
app.config['MYSQL_DATABASE_DB'] = 'racunarske_komponente'

mysql_db.init_app(app)

@app.route("/")
@app.route("/index")
def index():
    return app.send_static_file("index.html") 

@app.route("/komponente")
def dobavljanje_komponenti():
    cr = mysql_db.get_db().cursor()
    upit = "SELECT * FROM komponente"
    cr.execute(upit)
    komponente = cr.fetchall()
    return flask.json.jsonify(komponente)